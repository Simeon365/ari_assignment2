### A Pluto.jl notebook ###
# v0.18.4

using Markdown
using InteractiveUtils

# ╔═╡ 7de4ca62-caff-11ec-17d1-09b4a2875d2d
using Pkg

# ╔═╡ 2307b273-f039-4401-8793-571273174551
Pkg.activate("Project.toml")

# ╔═╡ 0541fb19-efe2-4d4c-8039-a2b452e1548d
	mutable struct Function
		Signature::Tuple{String, String}
		minimumResponseTime::Float64
		maximumLatency::Int64
		minimumThroughput::Int64
		minimumSuccessRate::Int64
	end

# ╔═╡ ee733e49-78d5-4c58-962a-cc120b196e82
	mutable struct Action
		Signature::Tuple{String, String}
		TempFunctions::Vector{Function}
	end

# ╔═╡ 2454978e-8da7-4677-b881-b6ea04337b1f
	mutable struct Arc
		position::Int64
		left::Tuple{Action,Function}
		right::Tuple{Action,Function}
	end

# ╔═╡ ba8f87c6-0d17-4ce7-be64-83d8008a69e8
	global listOfFunctions = []
	global collectionOfActions = []
	global ActionsConnections = []

# ╔═╡ f8f11a4e-9c87-4740-8e33-53044922ed72
function forwardChecking(d, a)
		for i in d
			if i.Signature == a.Signature
			end
		end
	end

# ╔═╡ 0704be8b-acb4-49b2-a8d6-f75bcda77fa0
function run()
	
	Domain = listOfFunctions;
	for i in collectionOfActions
		
		Domain = forwardChecking(Domain, i)
	end
	
end

# ╔═╡ Cell order:
# ╠═7de4ca62-caff-11ec-17d1-09b4a2875d2d
# ╠═2307b273-f039-4401-8793-571273174551
# ╠═0541fb19-efe2-4d4c-8039-a2b452e1548d
# ╠═ee733e49-78d5-4c58-962a-cc120b196e82
# ╠═2454978e-8da7-4677-b881-b6ea04337b1f
# ╠═ba8f87c6-0d17-4ce7-be64-83d8008a69e8
# ╠═f8f11a4e-9c87-4740-8e33-53044922ed72
# ╠═0704be8b-acb4-49b2-a8d6-f75bcda77fa0
