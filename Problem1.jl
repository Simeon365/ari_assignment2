### A Pluto.jl notebook ###
# v0.18.4

using Markdown
using InteractiveUtils

# ╔═╡ ea090cd0-ca0d-11ec-1ee5-11e5735c0a46
using Pkg

# ╔═╡ 83cae2ab-18a7-42f3-948c-8e3753ee5571
Pkg.activate("Project.toml")

# ╔═╡ e2e25def-0a2e-429b-b4e0-2eb56eb148de
using DataStructures

# ╔═╡ d7b4295c-ffb7-4db3-a9a9-99ee5add064c
using Markdown

# ╔═╡ 73266bd0-524c-4002-b0df-b93f5ed949c1
mutable struct Office
		itemCount::Int64 
		Position::Tuple{Int64, Int64}
	end

# ╔═╡ 1b47e089-02d8-42b3-89b4-b47ac533f2b0
struct Action
		Direction::String
		Move::Tuple{Int64, Int64}
		Cost::Int64
	end

# ╔═╡ eb950129-1e8c-40bd-8c78-39982db7a94b
mutable struct State
		Building::Matrix{Int64}
		StateCost::Int64
		collectedParcels::Int64
		CurrentOffice::Office
		path::Vector{String}
	end

# ╔═╡ de8549e0-aca2-4e56-b926-b5df97242987
begin
	global A1 = Action("Move Up",(-1,0)		, 1)
	global A2 = Action("Move Down",(1,0)	, 1)
	global A3 = Action("Move West",(0,-1)	, 2)
	global A4 = Action("Move East",(0,1)	, 2) 
	global A5 = Action("Collect",(0,0)      , 5)
end

# ╔═╡ 50995d31-2c09-40f8-aada-40c93516eb1e
global ParcelsToBeCollected =  4

# ╔═╡ fbfdea23-5a91-41c4-81aa-d0c0a3ddbf13
global CompanyX_Structure = 
	   [0 0 0;
        1 0 0; 
        0 1 0;
        1 1 0;
        0 0 0] 

# ╔═╡ 19ddd174-644c-4ade-ae4a-1c0ebd4f77bb
global start = Office(0, (1,1))

# ╔═╡ 388cc22d-5eb4-42e0-97a8-7091b4afb458
global initialState = State(CompanyX_Structure, 0, 0, start,  [])

# ╔═╡ fd9d7884-f9c2-497e-8f17-8ba680e94e99
begin
	global moves = 0
	global goal =  false
	global CanCollect = true 
	global checkVal = false 
	global validState = true
end

# ╔═╡ 185552f6-35d0-4c1c-a070-a9a81689a172
begin
	global PrevStatePosition = start.Position 
	global VisitedNodes = []
	push!(VisitedNodes, initialState)
	global CurrentState = () 
	global Path = []; 
end

# ╔═╡ 779a3704-0904-4a20-a552-390a8bbb0d28
function heuristic(m, cost, n ,nextMoveCost)
	
	gn = cost + nextMoveCost
	 if m[n[1],n[2]] >= 1 && CanCollect
		hn = (ParcelsToBeCollected - 1 ) * 3
	else
		hn = ParcelsToBeCollected * 3
	end
	fn = gn + hn
	return fn
end

# ╔═╡ 31a7862b-9651-43ec-99e6-60bac73c36b1
 function state_Is_Valid(PrevState, CurrentState)
	 
	 if PrevState.CurrentOffice.Position != start
		 if PrevState.CurrentOffice.Position == CurrentState.CurrentOffice.Position
			 if PrevState.collectedParcels == CurrentState.collectedParcels 
				 validState = false
			 end
		 end
	 end
	 validState = true
 end

# ╔═╡ eaadcc79-b0ae-4617-9a07-a526f0cce8f8
function getAvailableMovesList(TempState)

Available_Moves = Action[]
	
	tempP_A1 = 
		TempState.CurrentOffice.Position[1] + A1.Move[1] , 
		TempState.CurrentOffice.Position[2] + A1.Move[2] 

	tempP_A2 = 
		TempState.CurrentOffice.Position[1] + A2.Move[1] , 
		TempState.CurrentOffice.Position[2] + A2.Move[2] 
	
	tempP_A4 = 
		TempState.CurrentOffice.Position[1] + A4.Move[1] , 
		TempState.CurrentOffice.Position[2] + A4.Move[2] 
	
	tempP_A3 = 
		TempState.CurrentOffice.Position[1] + A3.Move[1] , 
		TempState.CurrentOffice.Position[2] + A3.Move[2] 

	n = TempState.CurrentOffice.Position
	
	if TempState.Building[n[1],n[2]] >= 1 && CanCollect
	push!(Available_Moves, A5)
	end
		
	if 1 <= tempP_A3[1] && 1 <= tempP_A3[2]
		push!(Available_Moves, A3)
	end
	
	if 1 <= tempP_A2[1] && 1 <= tempP_A2[2]
		push!(Available_Moves, A2)
	end
	
	if 1 <= tempP_A1[1] && 1 <= tempP_A1[2]
		push!(Available_Moves, A1)
	end
 
	if 1 <= tempP_A4[1] && 1 <= tempP_A4[2]
		push!(Available_Moves, A4)
	end
	
	return Available_Moves
end

# ╔═╡ 4b284e2b-f5c3-4f95-83e9-230f1152059f
function buildNewState(CompanyX_Structure,hurisiticVal,parcelsCollected,Coffice,Cpath, nextM, action)
	
	if action == A5
		x = CompanyX_Structure[Coffice.Position[1], Coffice.Position[2]]
		CompanyX_Structure[Coffice.Position[1], Coffice.Position[2]] = x - 1
		canCollect = false
	end
		
	a = []

	for i in Cpath
		push!(a, i)
	end

		push!(a, nextM)
	
	newState = State(CompanyX_Structure,hurisiticVal,parcelsCollected,Coffice,a)
	
	return newState
	end

# ╔═╡ a9bbacc7-b20c-4aaf-a9c6-a994b62c5bfc
function a_star(state)	
	stateList = []
	parcels = 0

	AvailableMoves = getAvailableMovesList(state) 

	for i in AvailableMoves 
		TempState = state;
		if i == A5

				println("Collecting...")
				
			 MX = TempState.Building
			 P_C = TempState.collectedParcels + 1
			 P =	
				(TempState.CurrentOffice.Position[1] + i.Move[1] , TempState.CurrentOffice.Position[2] + i.Move[2])
			 C_O = Office(TempState.Building[P[1],P[2]], P)
			 H_V = heuristic(TempState.Building,TempState.StateCost, P, i.Cost)
			 O_P = TempState.path
			 C_P =  i.Direction
							
			if state_Is_Valid(state, buildNewState(MX,H_V,P_C,C_O,O_P,C_P, i))
				push!(stateList, buildNewState(MX,H_V,P_C,C_O,O_P,C_P, i))
			end
			CanCollect = false;
		else
			MX = TempState.Building
			 P_C = TempState.collectedParcels
			 P =	
				(TempState.CurrentOffice.Position[1] + i.Move[1] , TempState.CurrentOffice.Position[2] + i.Move[2])
			 C_O = Office(TempState.Building[P[1],P[2]], P)
			 H_V = heuristic(TempState.Building,TempState.StateCost, P, i.Cost)
			 O_P = TempState.path
			 C_P =  i.Direction
		CanCollect = true;
			if state_Is_Valid(state, buildNewState(MX,H_V,P_C,C_O,O_P,C_P, i))
				push!(stateList, buildNewState(MX,H_V,P_C,C_O,O_P,C_P, i))
			end
		end
	end
	
	sort!(stateList, by = x -> x.StateCost) 
	sort!(stateList, by = x -> x.collectedParcels, rev=true) 
	return stateList
end

# ╔═╡ b907b697-a194-4a2d-aff1-725942363f68
function run(maze, state)
	TempState = state
	nodeList = []
		push!(nodeList, state)
	while nodeList[1].collectedParcels != ParcelsToBeCollected
	TempNodeList = a_star(nodeList[1]) 
	empty!(nodeList)
	for i in TempNodeList
		push!(nodeList, i)
	end
	end
	return nodeList[2]
end

# ╔═╡ 0cfe579e-0290-4951-a490-3ac3f3f2ca12
begin
	res = run(CompanyX_Structure, initialState)
	for i in res.path
		println(i)
	end
end

# ╔═╡ Cell order:
# ╠═ea090cd0-ca0d-11ec-1ee5-11e5735c0a46
# ╠═83cae2ab-18a7-42f3-948c-8e3753ee5571
# ╠═e2e25def-0a2e-429b-b4e0-2eb56eb148de
# ╠═d7b4295c-ffb7-4db3-a9a9-99ee5add064c
# ╠═73266bd0-524c-4002-b0df-b93f5ed949c1
# ╠═1b47e089-02d8-42b3-89b4-b47ac533f2b0
# ╠═eb950129-1e8c-40bd-8c78-39982db7a94b
# ╠═de8549e0-aca2-4e56-b926-b5df97242987
# ╠═50995d31-2c09-40f8-aada-40c93516eb1e
# ╠═fbfdea23-5a91-41c4-81aa-d0c0a3ddbf13
# ╠═19ddd174-644c-4ade-ae4a-1c0ebd4f77bb
# ╠═388cc22d-5eb4-42e0-97a8-7091b4afb458
# ╠═fd9d7884-f9c2-497e-8f17-8ba680e94e99
# ╠═185552f6-35d0-4c1c-a070-a9a81689a172
# ╠═779a3704-0904-4a20-a552-390a8bbb0d28
# ╠═31a7862b-9651-43ec-99e6-60bac73c36b1
# ╠═eaadcc79-b0ae-4617-9a07-a526f0cce8f8
# ╠═4b284e2b-f5c3-4f95-83e9-230f1152059f
# ╠═a9bbacc7-b20c-4aaf-a9c6-a994b62c5bfc
# ╠═b907b697-a194-4a2d-aff1-725942363f68
# ╠═0cfe579e-0290-4951-a490-3ac3f3f2ca12
